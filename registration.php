<?php
/**
 * Copyright © 2016 Cream. All rights reserved.
 * https://www.cream.nl/
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Cream_SecurePasswords',
    __DIR__ . '/src'
);
